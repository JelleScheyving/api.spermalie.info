<?php

class Form_Bestemming {
    // database connection and table name 
    private $conn; 
    // ####
    private $table_name = "tbl_form_bestemming"; 
    private $primary_key = "bestemmingID";
    private $sortField = "omschrijving";

    // object properties 
    // ####
    public $bestemmingID;
    public $omschrijving;
    public $best_tabel;
    public $best_veld;
    public $best_sleutel;


    // constructor with $db as database connection 
    public function __construct($db){ 
        $this->conn = $db;
    }

    function readAll(){
        // select all query
        $query = "SELECT * FROM " . $this->table_name . " 
                ORDER BY ".$this->sortField." ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readOne(){
        // query to read single record
        $query = "SELECT * FROM " . $this->table_name . "
                WHERE ".$this->primary_key." = ? LIMIT 0,1";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of product to be updated
        // #### pas naam primary key aan
        $stmt->bindParam(1, $this->bestemmingID);

        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        // ####
        $this->bestemmingID= $row['bestemmingID'];
        $this->omschrijving= $row['omschrijving'];
        $this->best_tabel= $row['best_tabel'];
        $this->best_veld= $row['best_veld'];
        $this->best_sleutel= $row['best_sleutel'];
    }
    
    function delete(){
        // delete query
        // #### pas ID aan
        $query = "DELETE FROM " . $this->table_name . 
                // #### pas naam primary key aan
                 " WHERE ".$this->bestemmingID." = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        // #### pas ID aan
        $this->bestemmingID=htmlspecialchars(strip_tags($this->bestemmingID));

        // bind id of record to delete
        // #### pas ID aan
        $stmt->bindParam(1, $this->bestemmingID);

        // execute query
        if($stmt->execute()){
            return true;
        }
        return false;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO 
                    " . $this->table_name . "
                SET ".
                "omschrijving=:omschrijving,
                best_tabel=:best_tabel,
                best_veld=:best_veld,
                best_sleutel=:best_sleutel
                ";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // posted values
        // ####
        $this->omschrijving=htmlspecialchars(strip_tags($this->omschrijving));
        $this->best_tabel=htmlspecialchars(strip_tags($this->best_tabel));
        $this->best_veld=htmlspecialchars(strip_tags($this->best_veld));
        $this->best_sleutel=htmlspecialchars(strip_tags($this->best_sleutel));

        // bind values
        // ####
        $stmt->bindParam(':omschrijving', $this->omschrijving);
        $stmt->bindParam(':best_tabel', $this->best_tabel);
        $stmt->bindParam(':best_veld', $this->best_veld);
        $stmt->bindParam(':best_sleutel', $this->best_sleutel);
        
        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        }else{
            echo "<pre>";
                print_r($stmt->errorInfo());
            echo "</pre>";

            return false;
        }
    }
}

