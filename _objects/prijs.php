<?php
class Prijs {
    // database connection and table name 
    private $conn; 
    private $table_name = "tbl_prijzen"; 
    private $primary_key = "prPrijsID";
    private $sort_field = "prVolgorde";
    private $eventID_field = "prEventID";

    // object properties 
    public $prPrijsID;
    public $prEventID;
    public $prNaam;
    public $prOmschrijving;
    public $prPrijs;
    public $prMaxDeelnemers;
    public $prHeader;
    public $prNaam_UnderScore;
    public $prVolgorde;


    // constructor with $db as database connection 
    public function __construct($db){ 
        $this->conn = $db;
    }

    function readAll(){
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . "  
                ORDER BY
                    " . $this->sort_field . " ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readOne(){
        // query to read single record
        $query = "SELECT 
                    *
                FROM 
                    " . $this->table_name . "
                WHERE 
                    ".$this->primary_key." = ? 
                LIMIT 
                    0,1";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of product to be read
        // #### pas prim. key aan
        $stmt->bindParam(1, $this->prPrijsID);

        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        $this->prPrijsID= $row['prPrijsID'];
        $this->prEventID= $row['prEventID'];
        $this->prNaam= $row['prNaam'];
        $this->prOmschrijving= $row['prOmschrijving'];
        $this->prPrijs= $row['prPrijs'];
        $this->prMaxDeelnemers= $row['prMaxDeelnemers'];
//        if($this->prMaxDeelnemers=="") {
//            $stmt->bindValue(':prMaxDeelnemers', null, PDO::PARAM_INT);
//        } else {
//            $stmt->bindParam(':prMaxDeelnemers', $this->prMaxDeelnemers);            
//        }   
        $this->prHeader= $row['prHeader'];
        $this->prNaam_UnderScore= $row['prNaam_UnderScore'];
        $this->prVolgorde= $row['prVolgorde'];
    }
    
    function readOneEvent($id){
        // #### pas veldnaam aan van veld waarin EventID wordt opgeslagen
        //$selectField = "prEventID";

        $query = "SELECT * FROM ".$this->table_name. 
                   " WHERE ".$this->eventID_field." = ? 
                    ORDER BY ".$this->sort_field." ASC";


        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of product to be updated
        $stmt->bindParam(1, $id);

        // execute query
        $stmt->execute();

        return $stmt;

        //return $query;
    }
    
    function delete(){
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE " . $this->primary_key. " = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        // #### pas ID aan
        $this->prPrijsID=htmlspecialchars(strip_tags($this->prPrijsID));

        // bind id of record to delete
        // #### pas ID aan
        $stmt->bindParam(1, $this->prPrijsID);

        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO 
                    " . $this->table_name . "
                SET 
                    prEventID=:prEventID,
                    prNaam=:prNaam,
                    prOmschrijving=:prOmschrijving,
                    prPrijs=:prPrijs,
                    prMaxDeelnemers=:prMaxDeelnemers,
                    prHeader=:prHeader,
                    prNaam_UnderScore=:prNaam_UnderScore,
                    prVolgorde=:prVolgorde
                    ";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // posted values
        $this->prEventID=htmlspecialchars(strip_tags($this->prEventID));
        $this->prNaam=htmlspecialchars(strip_tags($this->prNaam));
        $this->prOmschrijving=htmlspecialchars(strip_tags($this->prOmschrijving));
        $this->prPrijs=htmlspecialchars(strip_tags($this->prPrijs));
        if($this->prMaxDeelnemers=="") {
            $stmt->bindValue(':prMaxDeelnemers', null, PDO::PARAM_INT);
        } else {
            $stmt->bindParam(':prMaxDeelnemers', $this->prMaxDeelnemers);            
        }   
        //$this->prMaxDeelnemers=htmlspecialchars(strip_tags($this->prMaxDeelnemers));
        $this->prHeader=htmlspecialchars(strip_tags($this->prHeader));
        $this->prNaam_UnderScore=htmlspecialchars(strip_tags($this->prNaam_UnderScore));
        $this->prVolgorde=htmlspecialchars(strip_tags($this->prVolgorde));


        // bind values
        $stmt->bindParam(':prEventID', $this->prEventID);
        $stmt->bindParam(':prNaam', $this->prNaam);
        $stmt->bindParam(':prOmschrijving', $this->prOmschrijving);
        $stmt->bindParam(':prPrijs', $this->prPrijs);
        $stmt->bindParam(':prMaxDeelnemers', $this->prMaxDeelnemers);
        $stmt->bindParam(':prHeader', $this->prHeader);
        $stmt->bindParam(':prNaam_UnderScore', $this->prNaam_UnderScore);
        $stmt->bindParam(':prVolgorde', $this->prVolgorde);
        
        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        }else{
            echo "<pre>";
                print_r($stmt->errorInfo());
            echo "</pre>";

            return false;
        }
    }
}