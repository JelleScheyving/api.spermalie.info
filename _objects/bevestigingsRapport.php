<?php

class BevestigingsRapport {
    // database connection and table name 
    private $conn; 
    private $table_name = "tbl_bevestigingsrapport"; 
    private $primary_key = "RptBevestigingID";
    private $sortField = "RptBevestiging";

    // object properties 
    public $RptBevestigingID;
    public $RptBevestiging;

    // constructor with $db as database connection 
    public function __construct($db){ 
        $this->conn = $db;
    }
    
    function readAll(){
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . "  
                ORDER BY
                    " . $this->sortField . " ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // execute query
        $stmt->execute();

        return $stmt;
    }

    
    function readOne(){
        // query to read single record
        $query = "SELECT 
                    *
                FROM 
                    " . $this->table_name . "
                WHERE 
                    ".$this->primary_key." = ? 
                LIMIT 
                    0,1";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of instance
        // #### pas id aan
        $stmt->bindParam(1, $this->RptBevestigingID);

        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        $this->RptBevestigingID= $row['RptBevestigingID'];
        $this->RptBevestiging= $row['RptBevestiging'];
    }
    
    /*    
    function delete(){
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE " . $this->primary_key. " = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        // #### pas id aan
        $this->ID=htmlspecialchars(strip_tags($this->RptBevestigingID));

        // bind id of record to delete
        // #### pas id aan
        $stmt->bindParam(1, $this->RptBevestigingID);

        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }


    function create(){
        // query to insert record
        $query = "INSERT INTO 
                    " . $this->table_name . "
                SET 
                    RptBevestiging=:RptBevestiging
                    ";
        //####  OPGELET: geen komma na laatste waarde
        // prepare query
        $stmt = $this->conn->prepare($query);

        // posted values
        $this->RptBevestiging=htmlspecialchars(strip_tags($this->RptBevestiging));



        // bind values
        $stmt->bindParam(':RptBevestiging', $this->RptBevestiging);

        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        }else{
            echo "<pre>";
                print_r($stmt->errorInfo());
            echo "</pre>";

            return false;
        }
    }*/
}