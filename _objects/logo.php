<?php

class Logo {
    // database connection and table name 
    private $conn; 
    private $table_name = "tbl_logo"; 
    private $primary_key = "LogoID";
    private $sortField = "LogoNaam";

    // object properties 
    public $LogoID;
    public $LogoPad;
    public $LogoBestand;
    public $Voettekst;
    public $LogoNaam;


    // constructor with $db as database connection 
    public function __construct($db){ 
        $this->conn = $db;
    }
    
    function readAll(){
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . "  
                ORDER BY
                    " . $this->sortField . " ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // execute query
        $stmt->execute();

        return $stmt;
    }

    
    function readOne(){
        // query to read single record
        $query = "SELECT 
                    *
                FROM 
                    " . $this->table_name . "
                WHERE 
                    ".$this->primary_key." = ? 
                LIMIT 
                    0,1";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of instance
        // #### pas id aan
        $stmt->bindParam(1, $this->RptBevestigingID);

        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        $this->LogoID= $row['LogoID'];
        $this->LogoPad= $row['LogoPad'];
        $this->LogoBestand= $row['LogoBestand'];
        $this->Voettekst= $row['Voettekst'];
        $this->LogoNaam= $row['LogoNaam'];

    }
    
    /*    
    function delete(){
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE " . $this->primary_key. " = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        // #### pas id aan
        $this->ID=htmlspecialchars(strip_tags($this->LogoID));

        // bind id of record to delete
        // #### pas id aan
        $stmt->bindParam(1, $this->LogoID);

        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }


    function create(){
        // query to insert record
        $query = "INSERT INTO 
                    " . $this->table_name . "
                SET 
                    LogoPad=:LogoPad,
                    LogoBestand=:LogoBestand,
                    Voettekst=:Voettekst,
                    LogoNaam=:LogoNaam
                    ";
        //####  OPGELET: geen komma na laatste waarde
        // prepare query
        $stmt = $this->conn->prepare($query);

        // posted values
        $this->LogoPad=htmlspecialchars(strip_tags($this->LogoPad));
        $this->LogoBestand=htmlspecialchars(strip_tags($this->LogoBestand));
        $this->Voettekst=htmlspecialchars(strip_tags($this->Voettekst));
        $this->LogoNaam=htmlspecialchars(strip_tags($this->LogoNaam));

        // bind values
        $stmt->bindParam(':LogoPad', $this->LogoPad);
        $stmt->bindParam(':LogoBestand', $this->LogoBestand);
        $stmt->bindParam(':Voettekst', $this->Voettekst);
        $stmt->bindParam(':LogoNaam', $this->LogoNaam);

        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        }else{
            echo "<pre>";
                print_r($stmt->errorInfo());
            echo "</pre>";

            return false;
        }
    }
     
     */
}