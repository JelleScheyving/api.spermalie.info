<?php

class Betaling_View {
    // database connection and table name 
    private $conn; 
    private $table_name = "vwBetaling"; 
    //private $primary_key = "itemDepID";
    private $sort_field = "isInschrijvingsID";
    private $eventID_field = "isEventID";

    // object properties 
    public $isInschrijvingsID;
    public $isEventID;
    public $isBedragBetaald;
    public $isTafelNr;
    public $isReferentieBetaling;
    public $isAnnulatieSent;
    public $isInschrijvingRef;
    public $isTeBetalen;
    public $isTotaalAantal;
    public $dnNaam;
    public $dnVoornaam;
    public $fullname;
    public $evAfkortingNaam;
    public $evNaam;



    // constructor with $db as database connection 
    public function __construct($db){ 
        $this->conn = $db;
    }

    function readAll(){
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . "  
                ORDER BY
                    " . $this->sort_field . " ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readOneEvent($id){
        $query = "SELECT * FROM ".$this->table_name. 
                   " WHERE ".$this->eventID_field." = ? 
                    ORDER BY ".$this->sort_field." ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of product to be updated
        $stmt->bindParam(1, $id);

        // execute query
        $stmt->execute();

        return $stmt;
    }
    /*
    function readOne(){
        // query to read single record
        $query = "SELECT 
                    *
                FROM 
                    " . $this->table_name . "
                WHERE 
                    ".$this->primary_key." = ? 
                LIMIT 
                    0,1";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of product to be read
        // #### pas prim. key aan
        $stmt->bindParam(1, $this->itemDepID);

        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        $this->itemDepID= $row['itemDepID'];
        $this->parentID= $row['parentID'];
        $this->childID= $row['childID'];
        $this->valueItemID= $row['valueItemID'];
        $this->response= $row['response'];
        $this->itemDepEventID= $row['itemDepEventID'];
    }
    
    function delete(){
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE " . $this->primary_key. " = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        // #### pas ID 2 X aan
        $this->itemDepID=htmlspecialchars(strip_tags($this->itemDepID));

        // bind id of record to delete
        // #### pas ID aan
        $stmt->bindParam(1, $this->itemDepID);

        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO 
                    " . $this->table_name . "
                SET 
                    parentID=:parentID,
                    childID=:childID,
                    valueItemID=:valueItemID,
                    response=:response,
                    itemDepEventID=:itemDepEventID
                    ";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // posted values
        $this->parentID=htmlspecialchars(strip_tags($this->parentID));
        $this->childID=htmlspecialchars(strip_tags($this->childID));
        $this->valueItemID=htmlspecialchars(strip_tags($this->valueItemID));
        
        if($this->valueItemID=="") {
            $stmt->bindValue(':valueItemID', null, PDO::PARAM_INT);
        } else {
            $stmt->bindParam(':valueItemID', $this->valueItemID);            
        }   
        $this->response=htmlspecialchars(strip_tags($this->response));
        $this->itemDepEventID=htmlspecialchars(strip_tags($this->itemDepEventID));

        // bind values
        $stmt->bindParam(':parentID', $this->parentID);
        $stmt->bindParam(':childID', $this->childID);
        $stmt->bindParam(':valueItemID', $this->valueItemID);
        $stmt->bindParam(':response', $this->response);
        $stmt->bindParam(':itemDepEventID', $this->itemDepEventID);

        
        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        }else{
            echo "<pre>";
                print_r($stmt->errorInfo());
            echo "</pre>";

            return false;
        }
    }
     
     */
}