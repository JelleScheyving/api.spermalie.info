<?php

class VeldType {
    // database connection and table name 
    private $conn; 
    // ####
    private $table_name = "tbl_veldtype"; 
    private $primary_key = "veldTypeID";
    private $sortField = "veldType";

    // object properties 
    // ####
    public $veldTypeID;
    public $veldType;

    // constructor with $db as database connection 
    public function __construct($db){ 
        $this->conn = $db;
    }

    function readAll(){
        // select all query
        $query = "SELECT * FROM " . $this->table_name . " 
                ORDER BY ".$this->sortField." ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readOne(){
        // query to read single record
        $query = "SELECT * FROM " . $this->table_name . "
                WHERE ".$this->primary_key." = ? LIMIT 0,1";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of product to be updated
        // #### pas naam primary key aan
        $stmt->bindParam(1, $this->veldTypeID);

        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        // ####
        $this->veldTypeID= $row['veldTypeID'];
        $this->veldType= $row['veldType'];
    }
    
    function delete(){
        // delete query
        // #### pas ID aan
        $query = "DELETE FROM " . $this->table_name . 
                // #### pas naam primary key aan
                 " WHERE ".$this->veldTypeID." = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        // #### pas ID aan
        $this->veldTypeID=htmlspecialchars(strip_tags($this->veldTypeID));

        // bind id of record to delete
        // #### pas ID aan
        $stmt->bindParam(1, $this->veldTypeID);

        // execute query
        if($stmt->execute()){
            return true;
        }
        return false;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO 
                    " . $this->table_name . "
                SET ".
                // ####
                "veldType=:veldType
                ";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // posted values
        // ####
        $this->veldType=htmlspecialchars(strip_tags($this->veldType));

        // bind values
        // ####
        $stmt->bindParam(':veldType', $this->veldType);
        
        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        }else{
            echo "<pre>";
                print_r($stmt->errorInfo());
            echo "</pre>";

            return false;
        }
    }
}

