<?php
class Evenement {
    // database connection and table name 
    private $conn; 
    private $table_name = "tbl_evenement"; 
    private $primary_key = "evEventID";

    // object properties 
    public $evEventID;
    public $evNaam;
    public $evDatum;
    public $evAanvangsTijd;
    public $evEindDatumInschrijving;
    public $evOmschrijving;
    public $evMaxDeelnemers;
    public $evMaxInschrijvingen;
    public $evAfkortingNaam;
    public $evWachtwoord;
    public $evSlotformule;
    public $evActief;
    public $evTitelPrijzen;
    public $evHerinneringstermijn;
    public $evBetalingstermijn;
    public $evAnnulatieTermijn;
    public $evMailBody;
    public $evMailFrom;
    public $evMailSubject;
    public $evMailAttachment;
    public $evHerinneringBody1;
    public $evHerinneringSubject;
    public $evAnnuleringBody1;
    public $evAnnuleringSubject;
    public $evRptBevestigingID;
    public $evWachtlijst;
    public $evAanhefRapport;
    public $evTarieven;
    public $evActiefVanaf;
    public $evRekeningNummer;
    public $evMailBcc;
    public $evLogoID;
    public $evMailVerantwoordelijke;
    public $evCheckMaximum;
    public $evCheckNoMaxima;
    public $evSuccesBoodschap;
    public $evLogin;
    public $evBetalend;
    public $evBevestiging;
    public $evTabs;
    public $evCreated;
    public $evPrijsVerplicht;
    public $evAangemaakt;
    public $evAangepast;
    public $evReadonlyDN;
    public $evClusterKeuzeVraag;
    public $evClusterAantalVraag;

    // constructor with $db as database connection 
    public function __construct($db){ 
        $this->conn = $db;
    }

    function readAll(){
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . "  
                ORDER BY
                    " . $this->primary_key . " ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readOne(){
        // query to read single record
        $query = "SELECT 
                    *
                FROM 
                    " . $this->table_name . "
                WHERE 
                    ".$this->primary_key." = ? 
                LIMIT 
                    0,1";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of instance to be read
        $stmt->bindParam(1, $this->evEventID);

        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
            $this->evEventID= $row['evEventID'];
            $this->evNaam= $row['evNaam'];
            $this->evDatum= $row['evDatum'];
            $this->evAanvangsTijd= $row['evAanvangsTijd'];
            $this->evEindDatumInschrijving= $row['evEindDatumInschrijving'];
            $this->evOmschrijving= $row['evOmschrijving'];
            $this->evMaxDeelnemers= $row['evMaxDeelnemers'];
            $this->evMaxInschrijvingen= $row['evMaxInschrijvingen'];
            $this->evAfkortingNaam= $row['evAfkortingNaam'];
            $this->evWachtwoord= $row['evWachtwoord'];
            $this->evSlotformule= $row['evSlotformule'];
            $this->evActief= $row['evActief'];
            $this->evTitelPrijzen= $row['evTitelPrijzen'];
            $this->evHerinneringstermijn= $row['evHerinneringstermijn'];
            $this->evBetalingstermijn= $row['evBetalingstermijn'];
            $this->evAnnulatieTermijn= $row['evAnnulatieTermijn'];
            $this->evMailBody= $row['evMailBody'];
            $this->evMailFrom= $row['evMailFrom'];
            $this->evMailSubject= $row['evMailSubject'];
            $this->evMailAttachment= $row['evMailAttachment'];
            $this->evHerinneringBody1= $row['evHerinneringBody1'];
            $this->evHerinneringSubject= $row['evHerinneringSubject'];
            $this->evAnnuleringBody1= $row['evAnnuleringBody1'];
            $this->evAnnuleringSubject= $row['evAnnuleringSubject'];
            $this->evRptBevestigingID= $row['evRptBevestigingID'];
            $this->evWachtlijst= $row['evWachtlijst'];
            $this->evAanhefRapport= $row['evAanhefRapport'];
            $this->evTarieven= $row['evTarieven'];
            $this->evActiefVanaf= $row['evActiefVanaf'];
            $this->evRekeningNummer= $row['evRekeningNummer'];
            $this->evMailBcc= $row['evMailBcc'];
            $this->evLogoID= $row['evLogoID'];
            $this->evMailVerantwoordelijke= $row['evMailVerantwoordelijke'];
            $this->evCheckMaximum= $row['evCheckMaximum'];
            $this->evCheckNoMaxima= $row['evCheckNoMaxima'];
            $this->evSuccesBoodschap= $row['evSuccesBoodschap'];
            $this->evLogin= $row['evLogin'];
            $this->evBetalend= $row['evBetalend'];
            $this->evBevestiging= $row['evBevestiging'];
            $this->evTabs= $row['evTabs'];
            $this->evCreated= $row['evCreated'];
            $this->evPrijsVerplicht= $row['evPrijsVerplicht'];
            $this->evAangemaakt= $row['evAangemaakt'];
            $this->evAangepast= $row['evAangepast'];
            $this->evReadonlyDN= $row['evReadonlyDN'];
            $this->evClusterKeuzeVraag= $row['evClusterKeuzeVraag'];
            $this->evClusterAantalVraag= $row['evClusterAantalVraag'];
    }
    
    function delete(){
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE " . $this->primary_key. " = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->ID=htmlspecialchars(strip_tags($this->evEventID));

        // bind id of record to delete
        $stmt->bindParam(1, $this->evEventID);

        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO 
                    " . $this->table_name . "
                SET 
                    evNaam=:evNaam,
                    evDatum=:evDatum,
                    evAanvangsTijd=:evAanvangsTijd,
                    evEindDatumInschrijving=:evEindDatumInschrijving,
                    evOmschrijving=:evOmschrijving,
                    evMaxDeelnemers=:evMaxDeelnemers,
                    evMaxInschrijvingen=:evMaxInschrijvingen,
                    evAfkortingNaam=:evAfkortingNaam,
                    evWachtwoord=:evWachtwoord,
                    evSlotformule=:evSlotformule,
                    evActief=:evActief,
                    evTitelPrijzen=:evTitelPrijzen,
                    evHerinneringstermijn=:evHerinneringstermijn,
                    evBetalingstermijn=:evBetalingstermijn,
                    evAnnulatieTermijn=:evAnnulatieTermijn,
                    evMailBody=:evMailBody,
                    evMailFrom=:evMailFrom,
                    evMailSubject=:evMailSubject,
                    evMailAttachment=:evMailAttachment,
                    evHerinneringBody1=:evHerinneringBody1,
                    evHerinneringSubject=:evHerinneringSubject,
                    evAnnuleringBody1=:evAnnuleringBody1,
                    evAnnuleringSubject=:evAnnuleringSubject,
                    evRptBevestigingID=:evRptBevestigingID,
                    evWachtlijst=:evWachtlijst,
                    evAanhefRapport=:evAanhefRapport,
                    evTarieven=:evTarieven,
                    evActiefVanaf=:evActiefVanaf,
                    evRekeningNummer=:evRekeningNummer,
                    evMailBcc=:evMailBcc,
                    evLogoID=:evLogoID,
                    evMailVerantwoordelijke=:evMailVerantwoordelijke,
                    evCheckMaximum=:evCheckMaximum,
                    evCheckNoMaxima=:evCheckNoMaxima,
                    evSuccesBoodschap=:evSuccesBoodschap,
                    evLogin=:evLogin,
                    evBetalend=:evBetalend,
                    evBevestiging=:evBevestiging,
                    evTabs=:evTabs,
                    evCreated=:evCreated,
                    evPrijsVerplicht=:evPrijsVerplicht,
                    evAangemaakt=:evAangemaakt,
                    evAangepast=:evAangepast,
                    evReadonlyDN=:evReadonlyDN,
                    evClusterKeuzeVraag=:evClusterKeuzeVraag,
                    evClusterAantalVraag=:evClusterAantalVraag
                    ";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // posted values
        $this->evNaam=htmlspecialchars(strip_tags($this->evNaam));
        $this->evDatum=htmlspecialchars(strip_tags($this->evDatum));
        $this->evAanvangsTijd=htmlspecialchars(strip_tags($this->evAanvangsTijd));
        $this->evEindDatumInschrijving=htmlspecialchars(strip_tags($this->evEindDatumInschrijving));
        $this->evOmschrijving=htmlspecialchars(strip_tags($this->evOmschrijving));
        $this->evMaxDeelnemers=htmlspecialchars(strip_tags($this->evMaxDeelnemers));
        $this->evMaxInschrijvingen=htmlspecialchars(strip_tags($this->evMaxInschrijvingen));
        $this->evAfkortingNaam=htmlspecialchars(strip_tags($this->evAfkortingNaam));
        $this->evWachtwoord=htmlspecialchars(strip_tags($this->evWachtwoord));
        $this->evSlotformule=htmlspecialchars(strip_tags($this->evSlotformule));
        $this->evActief=htmlspecialchars(strip_tags($this->evActief));
        $this->evTitelPrijzen=htmlspecialchars(strip_tags($this->evTitelPrijzen));
        $this->evHerinneringstermijn=htmlspecialchars(strip_tags($this->evHerinneringstermijn));
        $this->evBetalingstermijn=htmlspecialchars(strip_tags($this->evBetalingstermijn));
        $this->evAnnulatieTermijn=htmlspecialchars(strip_tags($this->evAnnulatieTermijn));
        $this->evMailBody=htmlspecialchars(strip_tags($this->evMailBody));
        $this->evMailFrom=htmlspecialchars(strip_tags($this->evMailFrom));
        $this->evMailSubject=htmlspecialchars(strip_tags($this->evMailSubject));
        $this->evMailAttachment=htmlspecialchars(strip_tags($this->evMailAttachment));
        $this->evHerinneringBody1=htmlspecialchars(strip_tags($this->evHerinneringBody1));
        $this->evHerinneringSubject=htmlspecialchars(strip_tags($this->evHerinneringSubject));
        $this->evAnnuleringBody1=htmlspecialchars(strip_tags($this->evAnnuleringBody1));
        $this->evAnnuleringSubject=htmlspecialchars(strip_tags($this->evAnnuleringSubject));
        $this->evRptBevestigingID=htmlspecialchars(strip_tags($this->evRptBevestigingID));
        $this->evWachtlijst=htmlspecialchars(strip_tags($this->evWachtlijst));
        $this->evAanhefRapport=htmlspecialchars(strip_tags($this->evAanhefRapport));
        $this->evTarieven=htmlspecialchars(strip_tags($this->evTarieven));
        $this->evActiefVanaf=htmlspecialchars(strip_tags($this->evActiefVanaf));
        $this->evRekeningNummer=htmlspecialchars(strip_tags($this->evRekeningNummer));
        $this->evMailBcc=htmlspecialchars(strip_tags($this->evMailBcc));
        $this->evLogoID=htmlspecialchars(strip_tags($this->evLogoID));
        $this->evMailVerantwoordelijke=htmlspecialchars(strip_tags($this->evMailVerantwoordelijke));
        $this->evCheckMaximum=htmlspecialchars(strip_tags($this->evCheckMaximum));
        $this->evCheckNoMaxima=htmlspecialchars(strip_tags($this->evCheckNoMaxima));
        $this->evSuccesBoodschap=htmlspecialchars(strip_tags($this->evSuccesBoodschap));
        $this->evLogin=htmlspecialchars(strip_tags($this->evLogin));
        $this->evBetalend=htmlspecialchars(strip_tags($this->evBetalend));
        $this->evBevestiging=htmlspecialchars(strip_tags($this->evBevestiging));
        $this->evTabs=htmlspecialchars(strip_tags($this->evTabs));
        $this->evCreated=htmlspecialchars(strip_tags($this->evCreated));
        $this->evPrijsVerplicht=htmlspecialchars(strip_tags($this->evPrijsVerplicht));
        $this->evAangemaakt=htmlspecialchars(strip_tags($this->evAangemaakt));
        $this->evAangepast=htmlspecialchars(strip_tags($this->evAangepast));
        $this->evReadonlyDN=htmlspecialchars(strip_tags($this->evReadonlyDN));
        $this->evClusterKeuzeVraag=htmlspecialchars(strip_tags($this->evClusterKeuzeVraag));
        $this->evClusterAantalVraag=htmlspecialchars(strip_tags($this->evClusterAantalVraag));

        // bind values
        $stmt->bindParam(':evNaam', $this->evNaam);
        $stmt->bindParam(':evDatum', $this->evDatum);
        $stmt->bindParam(':evAanvangsTijd', $this->evAanvangsTijd);
        $stmt->bindParam(':evEindDatumInschrijving', $this->evEindDatumInschrijving);
        $stmt->bindParam(':evOmschrijving', $this->evOmschrijving);
        $stmt->bindParam(':evMaxDeelnemers', $this->evMaxDeelnemers);
        $stmt->bindParam(':evMaxInschrijvingen', $this->evMaxInschrijvingen);
        $stmt->bindParam(':evAfkortingNaam', $this->evAfkortingNaam);
        $stmt->bindParam(':evWachtwoord', $this->evWachtwoord);
        $stmt->bindParam(':evSlotformule', $this->evSlotformule);
        $stmt->bindParam(':evActief', $this->evActief);
        $stmt->bindParam(':evTitelPrijzen', $this->evTitelPrijzen);
        $stmt->bindParam(':evHerinneringstermijn', $this->evHerinneringstermijn);
        $stmt->bindParam(':evBetalingstermijn', $this->evBetalingstermijn);
        $stmt->bindParam(':evAnnulatieTermijn', $this->evAnnulatieTermijn);
        $stmt->bindParam(':evMailBody', $this->evMailBody);
        $stmt->bindParam(':evMailFrom', $this->evMailFrom);
        $stmt->bindParam(':evMailSubject', $this->evMailSubject);
        $stmt->bindParam(':evMailAttachment', $this->evMailAttachment);
        $stmt->bindParam(':evHerinneringBody1', $this->evHerinneringBody1);
        $stmt->bindParam(':evHerinneringSubject', $this->evHerinneringSubject);
        $stmt->bindParam(':evAnnuleringBody1', $this->evAnnuleringBody1);
        $stmt->bindParam(':evAnnuleringSubject', $this->evAnnuleringSubject);
        $stmt->bindParam(':evRptBevestigingID', $this->evRptBevestigingID);
        $stmt->bindParam(':evWachtlijst', $this->evWachtlijst);
        $stmt->bindParam(':evAanhefRapport', $this->evAanhefRapport);
        $stmt->bindParam(':evTarieven', $this->evTarieven);
        $stmt->bindParam(':evActiefVanaf', $this->evActiefVanaf);
        $stmt->bindParam(':evRekeningNummer', $this->evRekeningNummer);
        $stmt->bindParam(':evMailBcc', $this->evMailBcc);
        $stmt->bindParam(':evLogoID', $this->evLogoID);
        $stmt->bindParam(':evMailVerantwoordelijke', $this->evMailVerantwoordelijke);
        $stmt->bindParam(':evCheckMaximum', $this->evCheckMaximum);
        $stmt->bindParam(':evCheckNoMaxima', $this->evCheckNoMaxima);
        $stmt->bindParam(':evSuccesBoodschap', $this->evSuccesBoodschap);
        $stmt->bindParam(':evLogin', $this->evLogin);
        $stmt->bindParam(':evBetalend', $this->evBetalend);
        $stmt->bindParam(':evBevestiging', $this->evBevestiging);
        $stmt->bindParam(':evTabs', $this->evTabs);
        $stmt->bindParam(':evCreated', $this->evCreated);
        $stmt->bindParam(':evPrijsVerplicht', $this->evPrijsVerplicht);
        $stmt->bindParam(':evAangemaakt', $this->evAangemaakt);
        $stmt->bindParam(':evAangepast', $this->evAangepast);
        $stmt->bindParam(':evReadonlyDN', $this->evReadonlyDN);
        $stmt->bindParam(':evClusterKeuzeVraag', $this->evClusterKeuzeVraag);
        $stmt->bindParam(':evClusterAantalVraag', $this->evClusterAantalVraag);

        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        }else{
            echo "<pre>";
                print_r($stmt->errorInfo());
            echo "</pre>";

            return false;
        }
    }
}


