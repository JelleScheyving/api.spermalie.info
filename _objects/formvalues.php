<?php
class Formvalues {
    // database connection and table name 
    private $conn; 
    private $table_name = "tbl_form_values"; 
    private $primary_key = "ID";
    private $sortField = "waardeName";

    // object properties 
    public $ID; 
    public $waardeName; 
    public $waardeNum; 
    public $value_items_ID; 

    // constructor with $db as database connection 
    public function __construct($db){ 
        $this->conn = $db;
    }

    function readAll(){
        // select all query
        $query = "SELECT * FROM " . $this->table_name . " 
                ORDER BY ".$this->sortField." ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readOne(){
        // query to read single record
        $query = "SELECT * FROM " . $this->table_name . "
                WHERE ".$this->primary_key." = ? 
                LIMIT 0,1";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of product to be updated
        $stmt->bindParam(1, $this->ID);

        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        $this->ID = $row['ID'];
        $this->waardeName = $row['waardeName'];
        $this->waardeNum = $row['waardeNum'];
        $this->value_items_ID = $row['value_items_ID'];
    }
    
    function delete(){
        // delete query
        $query = "DELETE FROM " . $this->table_name . 
                 " WHERE ".$this->primary_key." = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->ID=htmlspecialchars(strip_tags($this->ID));

        // bind id of record to delete
        $stmt->bindParam(1, $this->ID);

        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO 
                    " . $this->table_name . "
                SET ".
                // ####
                "waardeName = :waardeName, 
                waardeNum = :waardeNum, 
                value_items_ID = :value_items_ID
                ";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // posted values
        // ####
        $this->waardeName=htmlspecialchars(strip_tags($this->waardeName));
        $this->waardeNum=htmlspecialchars(strip_tags($this->waardeNum));
        $this->value_items_ID=htmlspecialchars(strip_tags($this->value_items_ID));

        // bind values
        // ####
        $stmt->bindParam(':waardeName', $this->waardeName);
        $stmt->bindParam(':waardeNum', $this->waardeNum);
        $stmt->bindParam(':value_items_ID', $this->value_items_ID);
        
        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        }else{
            echo "<pre>";
                print_r($stmt->errorInfo());
            echo "</pre>";

            return false;
        }
    }
}