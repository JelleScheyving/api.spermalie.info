<?php

class Afh_Keuzes_View {
    // database connection and table name 
    private $conn; 
    private $table_name = "vwAfhankelijkKeuzes"; 
    //private $primary_key = "itemDepID";
    private $sort_field = "vraagID";
    private $eventID_field = "eventID";

    // object properties 
    public $vraagID;
    public $eventID;
    public $itemID;
    public $itemNaam;

    // constructor with $db as database connection 
    public function __construct($db){ 
        $this->conn = $db;
    }

    function readAll(){
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . "  
                ORDER BY
                    " . $this->sort_field . " ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readOneEvent($id){
        $query = "SELECT * FROM ".$this->table_name. 
                   " WHERE ".$this->eventID_field." = ? 
                    ORDER BY ".$this->sort_field." ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of product to be updated
        $stmt->bindParam(1, $id);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readOneQuestion($id){
        $query = "SELECT * FROM ".$this->table_name. 
                   " WHERE vraagID = ? 
                    ORDER BY ".$this->sort_field." ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of product to be updated
        $stmt->bindParam(1, $id);

        // execute query
        $stmt->execute();

        return $stmt;
    }
    
}