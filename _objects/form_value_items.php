<?php
class Form_value_items {
    // database connection and table name 
    private $conn; 
    private $table_name = "tbl_form_value_items"; 
    private $primary_key = "itemID";

    // object properties 
    public $itemID;
    public $valuesID;
    public $itemNaam;
    public $itemNum;
    public $itemEenheid;
    public $itemSorteer;
    public $itemMax;
    public $itemNow;

    // constructor with $db as database connection 
    public function __construct($db){ 
        $this->conn = $db;
    }
    
    function readAll(){
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . "  
                ORDER BY
                    " . $this->primary_key . " ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readOneFormValue($id){

        $query = "SELECT 
                    *
                FROM tbl_form_value_items 
                WHERE 
                    valuesID = ".$id. 
                " ORDER BY itemNaam ASC"; 
        $stmt = $this->conn->prepare( $query );

        $query = "SELECT * FROM tbl_form_value_items 
                    WHERE valuesID = ? 
                    ORDER BY itemNaam ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of product to be updated
        $stmt->bindParam(1, $id);

        // execute query
        $stmt->execute();

        return $stmt;
    }
    
    function readOne(){
        // query to read single record
        $query = "SELECT 
                    *
                FROM 
                    " . $this->table_name . "
                WHERE 
                    ".$this->primary_key." = ? 
                LIMIT 
                    0,1";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of product to be updated
        $stmt->bindParam(1, $this->itemID);

        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        $this->itemID= $row['itemID'];
        $this->valuesID= $row['valuesID'];
        $this->itemNaam= $row['itemNaam'];
        $this->itemNum= $row['itemNum'];
        $this->itemEenheid= $row['itemEenheid'];
        $this->itemSorteer= $row['itemSorteer'];
        $this->itemMax= $row['itemMax'];
        $this->itemNow= $row['itemNow'];
    }
    
    function delete(){
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE " . $this->primary_key. " = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->ID=htmlspecialchars(strip_tags($this->itemID));

        // bind id of record to delete
        $stmt->bindParam(1, $this->itemID);

        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO 
                    " . $this->table_name . "
                SET 
                    valuesID=:valuesID,
                    itemNaam=:itemNaam,
                    itemNum=:itemNum,
                    itemEenheid=:itemEenheid,
                    itemSorteer=:itemSorteer,
                    itemMax=:itemMax,
                    itemNow=:itemNow
                    ";
        //####  OPGELET: geen komma na laatste waarde
        // prepare query
        $stmt = $this->conn->prepare($query);

        // posted values
        $this->valuesID=htmlspecialchars(strip_tags($this->valuesID));
        $this->itemNaam=htmlspecialchars(strip_tags($this->itemNaam));
        $this->itemNum=htmlspecialchars(strip_tags($this->itemNum));
        $this->itemEenheid=htmlspecialchars(strip_tags($this->itemEenheid));
        $this->itemSorteer=htmlspecialchars(strip_tags($this->itemSorteer));
        $this->itemMax=htmlspecialchars(strip_tags($this->itemMax));
        $this->itemNow=htmlspecialchars(strip_tags($this->itemNow));


        // bind values
        $stmt->bindParam(':valuesID', $this->valuesID);
        $stmt->bindParam(':itemNaam', $this->itemNaam);
        $stmt->bindParam(':itemNum', $this->itemNum);
        $stmt->bindParam(':itemEenheid', $this->itemEenheid);
        $stmt->bindParam(':itemSorteer', $this->itemSorteer);
        $stmt->bindParam(':itemMax', $this->itemMax);
        $stmt->bindParam(':itemNow', $this->itemNow);

        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        }else{
            echo "<pre>";
                print_r($stmt->errorInfo());
            echo "</pre>";

            return false;
        }
    }
}