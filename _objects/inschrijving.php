<?php
class Inschrijving {
    // database connection and table name 
    private $conn; 
    private $table_name = "tbl_inschrijvingen_omzet"; 
    private $primary_key = "isInschrijvingsID";
    private $sort_field = "isInschrijvingsID";
    private $eventID_field = "isEventID";

    // object properties 
    public $isInschrijvingsID;
    public $isEventID;
    public $isDeelnemerID;
    public $isRegistratieTijd;
    public $isIPAdres;
    public $isTeBetalen;
    public $isBedragBetaald;
    public $isDatumBetaling;
    public $isReferentieBetaling;
    public $isMailSent;
    public $isHerinneringSent;
    public $isAnnulatieSent;
    public $isOpmerkingen;
    public $isInschrijvingRef;
    public $isVerontschuldigd;
    //public $isJSONData;
    public $isTotaalAantal;
    public $isTafelNr;
    public $isVerwijderd;
    public $isVerwijderdDatum;
    public $isVerwijderdUser;
    public $isClusterKeuze;
    public $isClusterAantal;

    // constructor with $db as database connection 
    public function __construct($db){ 
        $this->conn = $db;
    }

    function readAll(){
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . "  
                ORDER BY
                    " . $this->sort_field . " ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readOne($id){
        // query to read single record
        $query = "SELECT 
                    *
                FROM 
                    " . $this->table_name . "
                WHERE 
                    ".$this->primary_key." = ? 
                LIMIT 
                    0,1";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of record to be read
        // #### pas prim. key aan
        $stmt->bindParam(1, $id);

        // execute query
        $stmt->execute();

        return $stmt;
    }
    
    function readOneEvent($id){
        // #### pas veldnaam aan van veld waarin EventID wordt opgeslagen

        $query = "SELECT * FROM ".$this->table_name. 
                   " WHERE ".$this->eventID_field." = ? 
                    ORDER BY ".$this->sort_field." ASC";


        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of product to be updated
        $stmt->bindParam(1, $id);

        // execute query
        $stmt->execute();

        return $stmt;
        //return $query;
    }
    
    function delete(){
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE " . $this->primary_key. " = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        // #### pas ID aan
        $this->isInschrijvingsID=htmlspecialchars(strip_tags($this->isInschrijvingsID));

        // bind id of record to delete
        // #### pas ID aan
        $stmt->bindParam(1, $this->isInschrijvingsID);

        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO 
                    " . $this->table_name . "
                SET 
                    isEventID=:isEventID,
                    isDeelnemerID=:isDeelnemerID,
                    isRegistratieTijd=:isRegistratieTijd,
                    isIPAdres=:isIPAdres,
                    isTeBetalen=:isTeBetalen,
                    isBedragBetaald=:isBedragBetaald,
                    isDatumBetaling=:isDatumBetaling,
                    isReferentieBetaling=:isReferentieBetaling,
                    isMailSent=:isMailSent,
                    isHerinneringSent=:isHerinneringSent,
                    isAnnulatieSent=:isAnnulatieSent,
                    isOpmerkingen=:isOpmerkingen,
                    isInschrijvingRef=:isInschrijvingRef,
                    isVerontschuldigd=:isVerontschuldigd,
                    isTotaalAantal=:isTotaalAantal,
                    isTafelNr=:isTafelNr,
                    isVerwijderd=:isVerwijderd,
                    isVerwijderdDatum=:isVerwijderdDatum,
                    isVerwijderdUser=:isVerwijderdUser,
                    isClusterKeuze=:isClusterKeuze,
                    isClusterAantal=:isClusterAantal
                    ";
        
                     //isJSONData=:isJSONData,       

        // prepare query
        $stmt = $this->conn->prepare($query);

        // posted values
        $this->isEventID=htmlspecialchars(strip_tags($this->isEventID));
        $this->isDeelnemerID=htmlspecialchars(strip_tags($this->isDeelnemerID));
        $this->isRegistratieTijd=htmlspecialchars(strip_tags($this->isRegistratieTijd));
        $this->isIPAdres=htmlspecialchars(strip_tags($this->isIPAdres));
        $this->isTeBetalen=htmlspecialchars(strip_tags($this->isTeBetalen));
        $this->isBedragBetaald=htmlspecialchars(strip_tags($this->isBedragBetaald));
        $this->isDatumBetaling=htmlspecialchars(strip_tags($this->isDatumBetaling));
        $this->isReferentieBetaling=htmlspecialchars(strip_tags($this->isReferentieBetaling));
        $this->isMailSent=htmlspecialchars(strip_tags($this->isMailSent));
        $this->isHerinneringSent=htmlspecialchars(strip_tags($this->isHerinneringSent));
        $this->isAnnulatieSent=htmlspecialchars(strip_tags($this->isAnnulatieSent));
        $this->isOpmerkingen=htmlspecialchars(strip_tags($this->isOpmerkingen));
        $this->isInschrijvingRef=htmlspecialchars(strip_tags($this->isInschrijvingRef));
        $this->isVerontschuldigd=htmlspecialchars(strip_tags($this->isVerontschuldigd));
        //$this->isJSONData=htmlspecialchars(strip_tags($this->isJSONData));
        $this->isTotaalAantal=htmlspecialchars(strip_tags($this->isTotaalAantal));
        $this->isTafelNr=htmlspecialchars(strip_tags($this->isTafelNr));
        $this->isVerwijderd=htmlspecialchars(strip_tags($this->isVerwijderd));
        $this->isVerwijderdDatum=htmlspecialchars(strip_tags($this->isVerwijderdDatum));
        $this->isVerwijderdUser=htmlspecialchars(strip_tags($this->isVerwijderdUser));
        $this->isClusterKeuze=htmlspecialchars(strip_tags($this->isClusterKeuze));
        $this->isClusterAantal=htmlspecialchars(strip_tags($this->isClusterAantal));
        /*
        if($this->prMaxDeelnemers=="") {
            $stmt->bindValue(':prMaxDeelnemers', null, PDO::PARAM_INT);
        } else {
            $stmt->bindParam(':prMaxDeelnemers', $this->prMaxDeelnemers);            
        }   
         * 
         */

        // bind values
        $stmt->bindParam(':isEventID', $this->isEventID);
        $stmt->bindParam(':isDeelnemerID', $this->isDeelnemerID);
        $stmt->bindParam(':isRegistratieTijd', $this->isRegistratieTijd);
        $stmt->bindParam(':isIPAdres', $this->isIPAdres);
        $stmt->bindParam(':isTeBetalen', $this->isTeBetalen);
        $stmt->bindParam(':isBedragBetaald', $this->isBedragBetaald);
        $stmt->bindParam(':isDatumBetaling', $this->isDatumBetaling);
        $stmt->bindParam(':isReferentieBetaling', $this->isReferentieBetaling);
        $stmt->bindParam(':isMailSent', $this->isMailSent);
        $stmt->bindParam(':isHerinneringSent', $this->isHerinneringSent);
        $stmt->bindParam(':isAnnulatieSent', $this->isAnnulatieSent);
        $stmt->bindParam(':isOpmerkingen', $this->isOpmerkingen);
        $stmt->bindParam(':isInschrijvingRef', $this->isInschrijvingRef);
        $stmt->bindParam(':isVerontschuldigd', $this->isVerontschuldigd);
        //$stmt->bindParam(':isJSONData', $this->isJSONData);
        $stmt->bindParam(':isTotaalAantal', $this->isTotaalAantal);
        $stmt->bindParam(':isTafelNr', $this->isTafelNr);
        $stmt->bindParam(':isVerwijderd', $this->isVerwijderd);
        $stmt->bindParam(':isVerwijderdDatum', $this->isVerwijderdDatum);
        $stmt->bindParam(':isVerwijderdUser', $this->isVerwijderdUser);
        $stmt->bindParam(':isClusterKeuze', $this->isClusterKeuze);
        $stmt->bindParam(':isClusterAantal', $this->isClusterAantal);
        
        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        }else{
            echo "<pre>";
                print_r($stmt->errorInfo());
            echo "</pre>";

            return false;
        }
    }

    
    }