<?php

class Form {
    // database connection and table name 
    private $conn; 
    // ####
    private $table_name = "tbl_form"; 

    // object properties 
    // ####
    public $ID;
    public $eventID;
    public $volgorde;
    public $omschrijving;
    public $soort;
    public $waarde;
    public $verplicht;
    public $bestemming;
    public $maxDeelnemers;
    public $titel;
    public $omschrijvingUnderscore;
    public $vergelijkMet;


    // constructor with $db as database connection 
    public function __construct($db){ 
        $this->conn = $db;
    }

    function readAll(){
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . " 
                ORDER BY
                    ID ASC";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readOne(){
        // query to read single record
        $query = "SELECT * FROM " . $this->table_name . 
                // #### pas naam van de ID aan
                    "WHERE ID = ? LIMIT 0,1";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );

        // bind id of product to be updated
        $stmt->bindParam(1, $this->ID);

        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        // ####
        $this->ID= $row['ID'];
        $this->eventID= $row['eventID'];
        $this->volgorde= $row['volgorde'];
        $this->omschrijving= $row['omschrijving'];
        $this->soort= $row['soort'];
        $this->waarde= $row['waarde'];
        $this->verplicht= $row['verplicht'];
        $this->bestemming= $row['bestemming'];
        $this->maxDeelnemers= $row['maxDeelnemers'];
        $this->titel= $row['titel'];
        $this->omschrijvingUnderscore= $row['omschrijvingUnderscore'];
        $this->vergelijkMet= $row['vergelijkMet'];
    }
    
        function readOneEvent($eventID){
        // query to read single record
        $query = "SELECT * FROM " . $this->table_name . 
                " WHERE eventID = ?";
        
        //echo $query."\n";
        
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
        
        $stmt->bindParam(1, $eventID);
        
        // execute query
        $stmt->execute();

        return $stmt;

    }
    
    function delete(){
        // delete query
        // #### pas ID aan
        $query = "DELETE FROM " . $this->table_name . " WHERE ID = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->ID=htmlspecialchars(strip_tags($this->ID));

        // bind id of record to delete
        $stmt->bindParam(1, $this->ID);

        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO 
                    " . $this->table_name . "
                SET ".
                "eventID=:eventID,
                volgorde=:volgorde,
                omschrijving=:omschrijving,
                soort=:soort,
                waarde=:waarde,
                verplicht=:verplicht,
                bestemming=:bestemming,
                maxDeelnemers=:maxDeelnemers,
                titel=:titel,
                omschrijvingUnderscore=:omschrijvingUnderscore,
                vergelijkMet=:vergelijkMet
                ";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // posted values
        // ####
        $this->eventID=htmlspecialchars(strip_tags($this->eventID));
        $this->volgorde=htmlspecialchars(strip_tags($this->volgorde));
        $this->omschrijving=htmlspecialchars(strip_tags($this->omschrijving));
        $this->soort=htmlspecialchars(strip_tags($this->soort));
        $this->waarde=htmlspecialchars(strip_tags($this->waarde));
        $this->verplicht=htmlspecialchars(strip_tags($this->verplicht));
        $this->bestemming=htmlspecialchars(strip_tags($this->bestemming));
        $this->maxDeelnemers=htmlspecialchars(strip_tags($this->maxDeelnemers));
        $this->titel=htmlspecialchars(strip_tags($this->titel));
        $this->omschrijvingUnderscore=htmlspecialchars(strip_tags($this->omschrijvingUnderscore));
        $this->vergelijkMet=htmlspecialchars(strip_tags($this->vergelijkMet));

        // bind values
        // ####
        $stmt->bindParam(':eventID', $this->eventID);
        $stmt->bindParam(':volgorde', $this->volgorde);
        $stmt->bindParam(':omschrijving', $this->omschrijving);
        $stmt->bindParam(':soort', $this->soort);
        if($this->waarde=="") {
            //$stmt->bindParam(':waarde', 'null'); 
            $stmt->bindValue(':waarde', null, PDO::PARAM_INT);
        } else {
            $stmt->bindParam(':waarde', $this->waarde);            
        }
        $stmt->bindParam(':verplicht', $this->verplicht);
        if($this->bestemming=="") {
            $stmt->bindValue(':bestemming', null, PDO::PARAM_INT);
        } else {
            $stmt->bindParam(':bestemming', $this->bestemming);            
        }        
        //$stmt->bindParam(':bestemming', $this->bestemming);
        if($this->maxDeelnemers=="") {
            $stmt->bindValue(':maxDeelnemers', null, PDO::PARAM_INT);
        } else {
            $stmt->bindParam(':maxDeelnemers', $this->maxDeelnemers);            
        }        
        //$stmt->bindParam(':maxDeelnemers', $this->maxDeelnemers);
        $stmt->bindParam(':titel', $this->titel);
        $stmt->bindParam(':omschrijvingUnderscore', $this->omschrijvingUnderscore);
        $stmt->bindParam(':vergelijkMet', $this->vergelijkMet);
        
        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        }else{
            echo "<pre>";
                print_r($stmt->errorInfo());
            echo "</pre>";

            return false;
        }
    }
}

