<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object file 
include_once '../config/mysqli.php'; 
include_once '../_objects/formvalues.php'; 

// get database connection 
$mySqlIConnection = new MySqlIConnection(); 
$dbConnection = $mySqlIConnection->getConnection();
 
$tabelNaam = 'tbl_form_values';
$idName = "ID";
$idValue = 0;
$feedback = "OK";
 
$formvalues = new Formvalues($dbConnection);

$inputRecord = '{"ID":"1","waardeName":"RoomType","waardeNum":"0","value_items_ID":""}';
//$inputArray = '[{"ID":"1","waardeName":"RoomType","waardeNum":"0","value_items_ID":""}]';
$data = json_decode($inputRecord);   

//echo $data

//$array = $data[0];
$array = $data;

$sql = "UPDATE ".$tabelNaam." SET ";

foreach($array as $key => $value) {
  if($key == $idValue){
      $idValue = $value;
  } else {
      if ($value == '') {
          $sql= $sql.$key." = null,";    
      } else {      
          $sql= $sql."$key ='$value',";             
      }  
  }
}

$sql = substr($sql,0, strlen($sql) - 1 ).
        ' WHERE '.$idName." = ".$idValue;

if ($dbConnection->query($sql) === TRUE) {
    $feedback = '[{"update":"OK"}]';
} else {
    $feedback = '[{"update":"error"}]';
}

$dbConnection->close();

echo $feedback;