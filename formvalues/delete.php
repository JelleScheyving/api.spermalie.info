<?php 
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object file 
include_once '../config/database.php'; 
include_once '../_objects/formvalues.php'; 
 
// get database connection 
$database = new Database(); 
$db = $database->getConnection();
 
// prepare product object
$formvalues = new Formvalues($db);
 
// get product id
$data = json_decode(file_get_contents("php://input"));     
 
// set product id to be deleted
$formvalues->ID = $data->ID;
 
// delete the product
if($formvalues->delete()){
     echo '[{"deleted":"1"}]';
}
 
// if unable to delete the product
else{
     echo '[{"deleted":"0"}]';
}