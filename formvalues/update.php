<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object file 
include_once '../config/mysqli.php'; 
include_once '../_objects/formvalues.php'; 

// get database connection 
$mySqlIConnection = new MySqlIConnection(); 
$dbConnection = $mySqlIConnection->getConnection();
 
// get database connection 
$tabelNaam = 'tbl_form_values';
$idName = "ID";
$idValue = 0;
$feedback = "OK";
 
$formvalues = new Formvalues($dbConnection);

$input = file_get_contents("php://input");
$data = json_decode($input);   

//$array = $data[0];

$sql = "UPDATE ".$tabelNaam." SET ";

//foreach($array as $key => $value) {
foreach($data as $key => $value) {
  if($key == $idValue){
      $idValue = $value;
  } else {
      if ($value == '') {
          $sql= $sql.$key." = null,";    
      } else {      
          $sql= $sql."$key ='$value',";             
      }  
  }
}

$sql = substr($sql,0, strlen($sql) - 1 ).
        ' WHERE '.$idName." = ".$idValue;

if ($dbConnection->query($sql) === TRUE) {
    $feedback = '[{"update":"OK"}]';
} else {
    $feedback = '[{"update":"error"}]';
}

$dbConnection->close();

echo $feedback;