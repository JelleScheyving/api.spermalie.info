<?php
//Create formvalues
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object file
include_once '../config/database.php';
include_once '../_objects/formvalues.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate product object
$formvalues = new Formvalues($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// set form_value property values
$formvalues->waardeName = $data->waardeName;
//$formvalues->waardeNum = $data->waardeNum;
//$formvalues->value_items_ID = $data->value_items_ID;
 
//$formvalues->waardeName = "TestValue";
//$formvalues->waardeNum = 0;
//$formvalues->value_items_ID = null;

// create the product
$newid = $formvalues->create();

if(!$newid){
    // json format output
    echo '[{"newid":"error"}]';
}
// if able to create the product, tell the user
else{
    // json format output
    //echo '[{"newid":"' . $newid . '"}]';
    
    // create array
    $formvalues_arr = array(
        "ID" => $newid,
        "waardeName" => $formvalues->waardeName,
        "waardeNum" => $formvalues->waardeNum,
        "value_items_ID" => $formvalues->value_items_ID
    );
    sleep(1);
    // make it json format
    print_r(json_encode($formvalues_arr));
}