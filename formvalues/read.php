<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../_objects/formvalues.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$formvalues = new Formvalues($db);

// query products
$formvalues_stmt = $formvalues->readAll();
$num = $formvalues_stmt->rowCount();
 
$data="";
 
// check if more than 0 record found
if($num>0){
 
    $x=1;
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $formvalues_stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $data .= '{';
            $data .= '"ID":"'  . $ID . '",';
            $data .= '"waardeName":"'   . $waardeName . '",';
            $data .= '"waardeNum":"'   . $waardeNum . '",';
            $data .= '"value_items_ID":"' . $value_items_ID . '"';
        $data .= '}';
 
        $data .= $x<$num ? ',' : '';
 
        $x++;
    }
}
 
// json format output
echo '[' . $data . ']';