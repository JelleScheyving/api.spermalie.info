<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$className = 'formvalues';  

// include database and object files
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
$ID = htmlspecialchars($_GET["Id"]);
 
// initialize object
$formvalues = new Formvalues($db);
 
// get id of product to be read
$data = json_decode(file_get_contents("php://input"));     
 
// set ID property of product to be read
//#### $formvalues->ID = $data->ID;
$formvalues->ID = $ID;
 
// read the details of product to be read
$formvalues->readOne();
 
// create array
$formvalues_arr[] = array(
    "ID" => $formvalues->ID,
    "waardeName" => $formvalues->waardeName,
    "waardeNum" => $formvalues->waardeNum,
    "value_items_ID" => $formvalues->value_items_ID
);
 
// make it json format
print_r(json_encode($formvalues_arr));