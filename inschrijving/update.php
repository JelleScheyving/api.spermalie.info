<?php
//prijs
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// ####
$objectNaam = 'inschrijving';
$tabelNaam = 'tbl_inschrijvingen_omzet';
$idName = "isInschrijvingsID";

// include database and object file 
include_once '../config/mysqli.php'; 
include_once '../_objects/'.$objectNaam.'.php'; 

// get database connection 
$mySqlIConnection = new MySqlIConnection(); 
$dbConnection = $mySqlIConnection->getConnection();
 
// get database connection 

$idValue = 0;
$feedback = "OK";
 
// #### pas naam object aan
$instance = new Inschrijving($dbConnection);

$input = file_get_contents("php://input");
$data = json_decode($input);   

$sql = "UPDATE ".$tabelNaam." SET ";

foreach($data as $key => $value) {
  if($key == $idValue){
      $idValue = $value;
  } else {
      if ($value == '') {
          $sql= $sql.$key." = null,";    
      } else {
          //escape string om bv enkele aanhalingstekens te kunnen verwerken door een escape character ervoor te plaatsen
          $sql= $sql."$key ='".mysqli_real_escape_string($dbConnection, $value)."',";
      }  
  }
}

$sql = substr($sql,0, strlen($sql) - 1 ).
        ' WHERE '.$idName." = ".$idValue;

if ($dbConnection->query($sql) === TRUE) {
    $feedback = '{"update":"OK"}';
} else {
    $feedback = '{"update":"error"}';
}

$dbConnection->close();

echo $feedback." | ".$sql;