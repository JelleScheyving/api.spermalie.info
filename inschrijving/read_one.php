<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$className = 'inschrijving';

// include database and object files
include_once '../utilities/carriageReturn.php';
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
// #### pas naam object aan
$instance = new Inschrijving($db);
 
$Id = htmlspecialchars($_GET["ID"]);
//echo $eventId."\n";

// query products
$query = $instance->readOne($Id);
$num = $query->rowCount();

//echo "Aantal rijen: ".$num."\n";

$data="";

if($num>0){
 
    $x=1;
 
    while ($row = $query->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $data .= '{';
            $data .= '"isInschrijvingsID":"'.$isInschrijvingsID. '",';
            $data .= '"isEventID":"'.$isEventID. '",';
            $data .= '"isDeelnemerID":"'.$isDeelnemerID. '",';
            $data .= '"isRegistratieTijd":"'.$isRegistratieTijd. '",';
            $data .= '"isIPAdres":"'.$isIPAdres. '",';
            $data .= '"isTeBetalen":"'.$isTeBetalen. '",';
            $data .= '"isBedragBetaald":"'.$isBedragBetaald. '",';
            $data .= '"isDatumBetaling":"'.$isDatumBetaling. '",';
            $data .= '"isReferentieBetaling":"'.$isReferentieBetaling. '",';
            $data .= '"isMailSent":"'.$isMailSent. '",';
            $data .= '"isHerinneringSent":"'.$isHerinneringSent. '",';
            $data .= '"isAnnulatieSent":"'.$isAnnulatieSent. '",';
            $data .= '"isOpmerkingen":"'.$isOpmerkingen. '",';
            $data .= '"isInschrijvingRef":"'.$isInschrijvingRef. '",';
            $data .= '"isVerontschuldigd":"'.$isVerontschuldigd. '",';
            //$data .= '"isJSONData":"'.$isJSONData. '",';
            $data .= '"isTotaalAantal":"'.$isTotaalAantal. '",';
            $data .= '"isTafelNr":"'.$isTafelNr. '",';
            $data .= '"isVerwijderd":"'.$isVerwijderd. '",';
            $data .= '"isVerwijderdDatum":"'.$isVerwijderdDatum. '",';
            $data .= '"isVerwijderdUser":"'.$isVerwijderdUser. '",';
            $data .= '"isClusterKeuze":"'.$isClusterKeuze. '",';
            $data .= '"isClusterAantal":"'.$isClusterAantal. '"';
        $data .= '}';
 
        $data .= $x<$num ? ',' : '';
 
        $x++;
    }
}

//echo '[' . $data . ']';
echo $data;
