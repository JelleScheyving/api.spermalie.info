<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$className = 'logo';

// include database and object files
include_once '../utilities/carriageReturn.php';
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
// #### pas naam object aan
$instance = new Logo($db);

// query products
$arrInstances = $instance->readAll();
$num = $arrInstances->rowCount();
 
$data="";
 
// check if more than 0 record found
if($num>0){
 
    $x=1;
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $arrInstances->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $data .= '{';
            $data .= '"LogoID":"'.$LogoID. '",';
            $data .= '"LogoPad":"'.$LogoPad. '",';
            $data .= '"LogoBestand":"'.$LogoBestand. '",';
            $data .= '"Voettekst":"'.$Voettekst. '",';
            $data .= '"LogoNaam":"'.$LogoNaam. '"';
        $data .= '}';
 
        $data .= $x<$num ? ',' : '';
 
        $x++;
    }
}
 
// json format output
echo '[' . parse($data) . ']';