<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$className = 'prijs';

// include database and object files
include_once '../utilities/carriageReturn.php';
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$prijs = new Prijs($db);

// query products
$arrInstances = $prijs->readAll();
$num = $arrInstances->rowCount();
 
$data="";
 
// check if more than 0 record found
if($num>0){
 
    $x=1;
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $arrInstances->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $data .= '{';
            $data .= '"prPrijsID":"'.$prPrijsID. '",';
            $data .= '"prEventID":"'.$prEventID. '",';
            $data .= '"prNaam":"'.$prNaam. '",';
            $data .= '"prOmschrijving":"'.$prOmschrijving. '",';
            $data .= '"prPrijs":"'.$prPrijs. '",';
            $data .= '"prMaxDeelnemers":"'.$prMaxDeelnemers. '",';
            $data .= '"prHeader":"'.$prHeader. '",';
            $data .= '"prNaam_UnderScore":"'.$prNaam_UnderScore. '",';
            $data .= '"prVolgorde":"'.$prVolgorde. '"';
        $data .= '}';
 
        $data .= $x<$num ? ',' : '';
 
        $x++;
    }
}
 
// json format output
echo '[' . parse($data) . ']';