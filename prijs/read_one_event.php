<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$className = 'prijs';

// include database and object files
include_once '../utilities/carriageReturn.php';
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
// #### pas naam object aan
$instance = new Prijs($db);
 
//echo "1\n";

$eventId = htmlspecialchars($_GET["eventID"]);
//echo $eventId."\n";


 //query records
$query = $instance->readOneEvent($eventId);

//echo $query;


$num = $query->rowCount();

//echo "Aantal rijen: ".$num."\n";

$data="";

if($num>0){
 
    $x=1;
 
    while ($row = $query->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $data .= '{';
            $data .= '"prPrijsID":"'.$prPrijsID. '",';
            $data .= '"prEventID":"'.$prEventID. '",';
            $data .= '"prNaam":"'.$prNaam. '",';
            $data .= '"prOmschrijving":"'.$prOmschrijving. '",';
            $data .= '"prPrijs":"'.$prPrijs. '",';
            $data .= '"prMaxDeelnemers":"'.$prMaxDeelnemers. '",';
            $data .= '"prHeader":"'.$prHeader. '",';
            $data .= '"prNaam_UnderScore":"'.$prNaam_UnderScore. '",';
            $data .= '"prVolgorde":"'.$prVolgorde. '"';
        $data .= '}';
 
        $data .= $x<$num ? ',' : '';
 
        $x++;
    }
}

echo '[' . $data . ']';
