<?php
//create prijs
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object file
include_once '../config/database.php';
include_once '../_objects/prijs.php';
include_once '../utilities/controlMessage.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate object
$instance = new Prijs($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

// set property values
$instance->prPrijsID = $data->prPrijsID;
$instance->prEventID = $data->prEventID;
$instance->prNaam = $data->prNaam;
$instance->prOmschrijving = $data->prOmschrijving;
$instance->prPrijs = $data->prPrijs;
$instance->prMaxDeelnemers = $data->prMaxDeelnemers;
$instance->prHeader = $data->prHeader;
$instance->prNaam_UnderScore = $data->prNaam_UnderScore;
$instance->prVolgorde = $data->prVolgorde;


// create the instance
// #### pas naam id aan
$newid = $instance->create();
$instance->prPrijsID = $newid;

if(!$newid){
    // json format output
    echo '[{"newid":"error"}]';
}
// if able to create the product, tell the user
else{
    
    // create array
    $instances_arr = array(
        "prPrijsID "=> $prPrijsID,
        "prEventID "=> $prEventID,
        "prNaam "=> $prNaam,
        "prOmschrijving "=> $prOmschrijving,
        "prPrijs "=> $prPrijs,
        "prMaxDeelnemers "=> $prMaxDeelnemers,
        "prHeader "=> $prHeader,
        "prNaam_UnderScore "=> $prNaam_UnderScore,
        "prVolgorde "=> $prVolgorde            
    );
    sleep(1);
    // make it json format
    print_r(json_encode($instances_arr));
}