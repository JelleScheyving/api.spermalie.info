<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$className = 'prijs'; 

// include database and object files
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
$ID = htmlspecialchars($_GET["Id"]);
 
// initialize object
$instance = new Prijs($db);
 
// get id of product to be read
$data = json_decode(file_get_contents("php://input"));     
 
// set ID property of product to be read
$instance->prPrijsID = $ID;
 
// read the details of product to be read
$instance->readOne();
 
// create array
$instance_arr[] = array(
    "prPrijsID" => $instance->prPrijsID,
    "prEventID" => $instance->prEventID,
    "prNaam" => $instance->prNaam,
    "prOmschrijving" => $instance->prOmschrijving,
    "prPrijs" => $instance->prPrijs,
    "prMaxDeelnemers" => $instance->prMaxDeelnemers,
    "prHeader" => $instance->prHeader,
    "prNaam_UnderScore" => $instance->prNaam_UnderScore,
    "prVolgorde" => $instance->prVolgorde
);
 
// make it json format
print_r(json_encode($instance_arr));