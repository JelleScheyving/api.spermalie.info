<?php 
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object file 
include_once '../config/database.php'; 
include_once '../_objects/prijs.php'; 
 
// get database connection 
$database = new Database(); 
$db = $database->getConnection();
 
// prepare object
$prijs = new Prijs($db);
 
// get object
$input = '{"ID":"194"}';
$data = json_decode($input);     
 
// set id of item to be deleted
$prijs->prPrijsID = $data->ID;
 
// delete the product
if($prijs->delete()){
     echo '[{"deleted":"1"}]';
}
 
// if unable to delete the product
else{
     echo '[{"deleted":"0"}]';
}