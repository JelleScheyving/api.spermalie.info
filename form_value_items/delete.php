<?php 
//verwijder form_value_item
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");


$className = 'form_value_items';
// include database and object file 
include_once '../config/database.php'; 
include_once '../_objects/'.$className.'.php'; 
 
// get database connection 
$database = new Database(); 
$db = $database->getConnection();
 
// prepare instance
$instance = new Form_value_items($db);
 
// get object
$data = json_decode(file_get_contents("php://input"));     
 
// set id of item to be deleted
$instance->itemID = $data->itemID;
 
// delete the product
if($instance->delete()){
     echo '[{"deleted":"1"}]';
}
 
// if unable to delete the product
else{
     echo '[{"deleted":"0"}]';
}