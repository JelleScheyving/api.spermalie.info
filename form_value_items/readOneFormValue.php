<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../_objects/form_value_items.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
$id = htmlspecialchars($_GET["id"]);
 
// initialize object
$form_value_items = new Form_value_items($db);

// query products
$query = $form_value_items->readOneFormValue($id);
$num = $query->rowCount();

//echo "Aantal rijen: ".$num."\n";

$data="";

if($num>0){
 
    $x=1;
 
    while ($row = $query->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $data .= '{';
            $data .= '"itemID":"'.$itemID. '",';
            $data .= '"valuesID":"'.$valuesID. '",';
            $data .= '"itemNaam":"'.$itemNaam. '",';
            $data .= '"itemNum":"'.$itemNum. '",';
            $data .= '"itemEenheid":"'.$itemEenheid. '",';
            $data .= '"itemSorteer":"'.$itemSorteer. '",';
            $data .= '"itemMax":"'.$itemMax. '",';
            $data .= '"itemNow":"'.$itemNow. '"';
        $data .= '}';
 
        $data .= $x<$num ? ',' : '';
 
        $x++;
    }
}

echo '[' . $data . ']';