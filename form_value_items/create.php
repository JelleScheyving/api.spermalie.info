<?php
//Create form_value_items
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$className = 'form_value_items';

// include database and object file
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
include_once '../utilities/controlMessage.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate object
$instance = new Form_value_items($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

// set property values
$instance->itemID = $data->itemID;
$instance->valuesID = $data->valuesID;
$instance->itemNaam = $data->itemNaam;
$instance->itemNum = $data->itemNum;
$instance->itemEenheid = $data->itemEenheid;
$instance->itemSorteer = $data->itemSorteer;
$instance->itemMax = $data->itemMax;
$instance->itemNow = $data->itemNow;

// create the instance
$newid = $instance->create();

if(!$newid){
    // json format output
    echo '[{"newid":"error"}]';
}
// if able to create the product, tell the user
else{
    
    // create array
    $instances_arr = array(
        "itemID "=> $newid,
        "valuesID "=> $valuesID,
        "itemNaam "=> $itemNaam,
        "itemNum "=> $itemNum,
        "itemEenheid "=> $itemEenheid,
        "itemSorteer "=> $itemSorteer,
        "itemMax "=> $itemMax,
        "itemNow "=> $itemNow
    );
    sleep(1);
    // make it json format
    print_r(json_encode($instances_arr));
}