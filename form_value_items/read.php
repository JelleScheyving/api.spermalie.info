<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
include_once '../config/database.php';
include_once '../_objects/form_value_items.php';

$database = new Database();
$db = $database->getConnection();
 
// initialize object
$form_value_items = new Form_value_items($db);

// query products
$query = $form_value_items->readAll();
$num = $query->rowCount();
 
$data="";

if($num>0){
 
    $x=1;
 
    while ($row = $query->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $data .= '{';
            $data .= '"itemID":"'.$itemID. '",';
            $data .= '"valuesID":"'.$valuesID. '",';
            $data .= '"itemNaam":"'.$itemNaam. '",';
            $data .= '"itemNum":"'.$itemNum. '",';
            $data .= '"itemEenheid":"'.$itemEenheid. '",';
            $data .= '"itemSorteer":"'.$itemSorteer. '",';
            $data .= '"itemMax":"'.$itemMax. '",';
            $data .= '"itemNow":"'.$itemNow. '"';
        $data .= '}';
 
        $data .= $x<$num ? ',' : '';
 
        $x++;
    }
}
 
 //json format output
echo '[' . $data . ']';