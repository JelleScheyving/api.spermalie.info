<?php

//Create form_value_items
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$className = 'form_value_items';

// include database and object file
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
include_once '../utilities/controlMessage.php';
 

$database = new Database();
$db = $database->getConnection();

// instantiate object
$instance = new Form_value_items($db);

$inputRecord = '{"itemID": "0", "valuesID": "155", "itemNaam": "Test", "itemNum": "0", "itemEenheid": "kg", "itemMax": "50", "itemNow": "0"}';
$data = json_decode($inputRecord); 

// set property values
$instance->itemID = $data->itemID;
$instance->valuesID = $data->valuesID;
$instance->itemNaam = $data->itemNaam;
$instance->itemNum = $data->itemNum;
$instance->itemEenheid = $data->itemEenheid;
$instance->itemSorteer = $data->itemSorteer;
$instance->itemMax = $data->itemMax;
$instance->itemNow = $data->itemNow;

// create the instance
$newid = $instance->create();

if(!$newid){
    // json format output
    echo '[{"newid":"error"}]';
}

// if able to create the instance, tell the user
else{
    echo $newid;
}
  /*    
    // create array
    $instances_arr = array(
        "itemID "=> $itemID,
        "valuesID "=> $valuesID,
        "itemNaam "=> $itemNaam,
        "itemNum "=> $itemNum,
        "itemEenheid "=> $itemEenheid,
        "itemSorteer "=> $itemSorteer,
        "itemMax "=> $itemMax,
        "itemNow "=> $itemNow
    );
    sleep(1);
    // make it json format
    print_r(json_encode($instances_arr));
}

*/



