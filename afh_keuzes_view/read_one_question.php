<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$className = 'afh_keuzes_view';

// include database and object files
include_once '../utilities/carriageReturn.php';
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
// #### pas naam object aan
$instance = new Afh_Keuzes_View($db);
 
$formId = htmlspecialchars($_GET["formID"]);

 //query records
$query = $instance->readOneQuestion($formId);

$num = $query->rowCount();

$data="";

if($num>0){
 
    $x=1;
 
    while ($row = $query->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $data .= '{';
            $data .= '"vraagID":"'.$vraagID. '",';
            $data .= '"eventID":"'.$eventID. '",';
            $data .= '"itemID":"'.$itemID. '",';
            $data .= '"itemNaam":"'.$itemNaam. '"';
        $data .= '}';
 
        $data .= $x<$num ? ',' : '';
 
        $x++;
    }
}

echo '[' . $data . ']';
