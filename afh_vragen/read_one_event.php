<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$className = 'afh_vragen';

// include database and object files
include_once '../utilities/carriageReturn.php';
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
// #### pas naam object aan
$instance = new Afh_Vragen($db);
 
$eventId = htmlspecialchars($_GET["eventID"]);

 //query records
$query = $instance->readOneEvent($eventId);

$num = $query->rowCount();

$data="";

if($num>0){
 
    $x=1;
 
    while ($row = $query->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $data .= '{';
            $data .= '"itemDepID":"'.$itemDepID. '",';
            $data .= '"itemDepEventID":"'.$itemDepEventID. '",';
            $data .= '"AntwoordOpVraag":"'.$AntwoordOpVraag. '",';
            $data .= '"Antwoord":"'.$Antwoord. '",';
            $data .= '"ToonVraag":"'.$ToonVraag. '",';
            $data .= '"childID":"'.$childID. '",';
            $data .= '"parentID":"'.$parentID. '",';
            $data .= '"valueItemID":"'.$valueItemID. '",';
            $data .= '"response":"'.$response. '"';
        $data .= '}';
 
        $data .= $x<$num ? ',' : '';
 
        $x++;
    }
}

echo '[' . $data . ']';
