<?php
//create afhankelijke vraag
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$className = 'afh_vragen';

// include database and object file
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
include_once '../utilities/controlMessage.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate object
$instance = new Afh_Vragen($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

// set property values
$instance->itemDepID = $data->itemDepID;
$instance->parentID = $data->parentID;
$instance->childID = $data->childID;
$instance->valueItemID = $data->valueItemID;
$instance->response = $data->response;
$instance->itemDepEventID = $data->itemDepEventID;

// create the instance
// #### pas naam id aan
$newid = $instance->create();
$instance->itemDepID = $newid;

if(!$newid){
    // json format output
    echo '[{"newid":"error"}]';
}
// if able to create the product, tell the user
else{
    
    // create array
    $instances_arr = array(
        "itemDepID "=> $itemDepID,
        "parentID "=> $parentID,
        "childID "=> $childID,
        "valueItemID "=> $valueItemID,
        "response "=> $response,
        "itemDepEventID "=> $itemDepEventID
    );
    sleep(1);
    // make it json format
    print_r(json_encode($instances_arr));
}