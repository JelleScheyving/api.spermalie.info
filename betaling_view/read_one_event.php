<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$className = 'betaling_view';

// include database and object files
include_once '../utilities/carriageReturn.php';
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
// #### pas naam object aan
$instance = new Betaling_View($db);
 
$eventId = htmlspecialchars($_GET["eventID"]);

 //query records
$query = $instance->readOneEvent($eventId);

$num = $query->rowCount();

$data="";

if($num>0){
 
    $x=1;
 
    while ($row = $query->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $data .= '{';
            $data .= '"isInschrijvingsID":"'.$isInschrijvingsID.'",';
            $data .= '"isEventID":"'.$isEventID.'",';
            $data .= '"isBedragBetaald":"'.$isBedragBetaald.'",';
            $data .= '"isTafelNr":"'.$isTafelNr.'",';
            $data .= '"isReferentieBetaling":"'.$isReferentieBetaling.'",';
            $data .= '"isAnnulatieSent":"'.$isAnnulatieSent.'",';
            $data .= '"isInschrijvingRef":"'.$isInschrijvingRef.'",';
            $data .= '"isTeBetalen":"'.$isTeBetalen.'",';
            $data .= '"isTotaalAantal":"'.$isTotaalAantal.'",';
            $data .= '"dnNaam":"'.$dnNaam.'",';
            $data .= '"dnVoornaam":"'.$dnVoornaam.'",';
            $data .= '"fullname":"'.$fullname.'",';
            $data .= '"evAfkortingNaam":"'.$evAfkortingNaam.'",';
            $data .= '"evNaam":"'.$evNaam.'"';
        $data .= '}';
 
        $data .= $x<$num ? ',' : '';
 
        $x++;
    }
}

echo '[' . $data . ']';
