<?php 
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$className = 'form';

// include database and object file 
include_once '../config/database.php'; 
include_once '../_objects/'.$className.'.php';
 
// get database connection 
$database = new Database(); 
$db = $database->getConnection();
 
// prepare object
// ####
$instance = new Form($db);
 
// get product id
$data = json_decode(file_get_contents("php://input"));     
 
// set instance id to be deleted
$instance->ID = $data->ID;
 
// delete the product
if($instance->delete()){
     echo '[{"deleted":"1"}]';
}
 
// if unable to delete the instance
else{
     echo '[{"deleted":"0"}]';
}