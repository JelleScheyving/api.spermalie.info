<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// ####
$className = 'form';  
$primKeyName = 'ID';

// include database and object files
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
$ID = htmlspecialchars($_GET[$primKeyName]);
 
// initialize object
// #### pas naam object aan
$instance = new Form($db);
 
// get id of product to be read
$data = json_decode(file_get_contents("php://input"));     
 
// set ID property of instance to be read
//#### 
$instance->ID = $ID;
 
// read the details of the record to be read
// ####
$form->readOne();
 
// create array
$instance_arr[] = array(
    "ID" => $instance->ID,
    "eventID" => $instance->eventID,
    "volgorde" => $instance->volgorde,
    "omschrijving" => $instance->omschrijving,
    "soort" => $instance->soort,
    "waarde" => $instance->waarde,
    "verplicht" => $instance->verplicht,
    "bestemming" => $instance->bestemming,
    "maxDeelnemers" => $instance->maxDeelnemers,
    "titel" => $instance->titel,
    "omschrijvingUnderscore" => $instance->omschrijvingUnderscore,
    "vergelijkMet" => $instance->vergelijkMet
);
 
// make it json format
print_r(json_encode($instance_arr));