<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$className = 'form';

// include database and object files
include_once '../utilities/carriageReturn.php';
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
// #### pas naam object aan
$instance = new Form($db);
 
//echo "1\n";

$eventId = htmlspecialchars($_GET["eventID"]);
//echo $eventId."\n";

// query products
$query = $instance->readOneEvent($eventId);
$num = $query->rowCount();

//echo "Aantal rijen: ".$num."\n";

$data="";

if($num>0){
 
    $x=1;
 
    while ($row = $query->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $data .= '{';
            $data .= '"ID":"'.$ID. '",';
            $data .= '"eventID":"'.$eventID. '",';
            $data .= '"volgorde":"'.$volgorde. '",';
            $data .= '"omschrijving":"'.$omschrijving. '",';
            $data .= '"soort":"'.$soort. '",';
            $data .= '"waarde":"'.$waarde. '",';
            $data .= '"verplicht":"'.$verplicht. '",';
            $data .= '"bestemming":"'.$bestemming. '",';
            $data .= '"maxDeelnemers":"'.$maxDeelnemers. '",';
            $data .= '"titel":"'.$titel. '",';
            $data .= '"omschrijvingUnderscore":"'.$omschrijvingUnderscore. '",';
            $data .= '"vergelijkMet":"'.$vergelijkMet. '"';
        $data .= '}';
 
        $data .= $x<$num ? ',' : '';
 
        $x++;
    }
}

echo '[' . $data . ']';
