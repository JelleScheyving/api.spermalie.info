<?php
//create form (vragen)
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// ####
$className = 'form';
$separator = "--";

// include database and object file
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
include_once '../utilities/controlMessage.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate product object
// ####
$instance = new Form($db);
 
// get posted data
//$data = json_decode(file_get_contents("php://input"));

$input = '{"ID":null,'
        . '"eventID":"213",'
        . '"volgorde":1,'
        . '"omschrijving":"Test1208",'
        . '"soort":"check",'
        . '"waarde":null,'
        . '"verplicht":"J",'
        . '"bestemming":null,'
        . '"maxDeelnemers":null,'
        . '"titel":"",'
        . '"omschrijvingUnderscore":"Test",'
        . '"vergelijkMet":""}';

/*
$input = '{"ID":"",'
        . '"eventID":"213",'
        . '"volgorde":1,'
        . '"omschrijving":"TestNull",'
        . '"soort":"check",'
        . '"waarde":"",'
        . '"verplicht":"J",'
        . '"bestemming":"",'
        . '"maxDeelnemers":"",'
        . '"titel":"",'
        . '"omschrijvingUnderscore":"Test",'
        . '"vergelijkMet":""}';
 */
$input = str_replace('null', '""', $input);
//$data = json_decode(file_get_contents("php://input"));

echo $input."\n";

$data = json_decode($input);


echo '$data->eventID: '.$data->eventID;
// set event property values
// ####
$instance->ID = $data->ID;
$instance->eventID = $data->eventID;

$instance->volgorde = $data->volgorde;
$instance->omschrijving = $data->omschrijving;
$instance->soort = $data->soort;
$instance->waarde = $data->waarde;
$instance->verplicht = $data->verplicht;
$instance->bestemming = $data->bestemming;
$instance->maxDeelnemers = $data->maxDeelnemers;
$instance->titel = $data->titel;
$instance->omschrijvingUnderscore = $data->omschrijvingUnderscore;
$instance->vergelijkMet = $data->vergelijkMet;

// create the instance
$newid = $instance->create();
$instance->ID = $newid;

if(!$newid){
    // json format output
    echo '[{"newid":"error"}]';
}
// if able to create the instance, give feedback
else{
    // create array
    /*
    $instances_arr = array(
        "ID "=> $ID,
        "eventID "=> $eventID,
        "volgorde "=> $volgorde,
        "omschrijving "=> $omschrijving,
        "soort "=> $soort,
        "waarde "=> $waarde,
        "verplicht "=> $verplicht,
        "bestemming "=> $bestemming,
        "maxDeelnemers "=> $maxDeelnemers,
        "titel "=> $titel,
        "omschrijvingUnderscore "=> $omschrijvingUnderscore,
        "vergelijkMet "=> $vergelijkMet
    );
     
     */

    sleep(1);
    echo $separator."\n";
    echo '{"newid":"'.$newid.'"}';    
    // make it json format
    echo $separator."\n";echo $separator."\n";
    print_r('['.json_encode($instance)).']';
}