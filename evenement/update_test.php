<?php
//evenement
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$objectNaam = 'evenement';
$tabelNaam = 'tbl_evenement';
$idName = "evEventID";

// include database and object file 
include_once '../config/mysqli.php'; 
include_once '../_objects/'.$objectNaam.'.php'; 

// get database connection 
$mySqlIConnection = new MySqlIConnection(); 
$dbConnection = $mySqlIConnection->getConnection();
 
// get database connection 

$idValue = 0;
$feedback = "OK";
 
$instance = new Evenement($dbConnection);

$input = '{"evEventID":"213","evNaam":"Nieuwste testje","evDatum":"","evAanvangsTijd":"","evEindDatumInschrijving":"","evOmschrijving":"Nieuw event","evMaxDeelnemers":"","evMaxInschrijvingen":"","evAfkortingNaam":"Nieuw","evWachtwoord":"Nieuw","evSlotformule":"","evActief":"J","evTitelPrijzen":"","evHerinneringstermijn":"","evBetalingstermijn":"","evAnnulatieTermijn":"3","evMailBody":"","evMailFrom":"","evMailSubject":"Onderwerp van de mail","evMailAttachment":"","evHerinneringBody1":"","evHerinneringSubject":"Onderwerp van de herinneringsmail","evAnnuleringBody1":"","evAnnuleringSubject":"Onderwerp van de annuleringsmail","evRptBevestigingID":"3","evWachtlijst":"Ni","evAanhefRapport":"","evTarieven":"","evActiefVanaf":"2018-03-16 14:10:38","evRekeningNummer":"","evMailBcc":"","evLogoID":"1","evMailVerantwoordelijke":"info@spermalie.be","evCheckMaximum":"1","evCheckNoMaxima":"0","evSuccesBoodschap":"We hebben uw inschrijving goed ontvangen. U ontvangt een bevestigingsmail.","evLogin":"N","evBetalend":"0","evBevestiging":"0","evTabs":"Afhankelijk, Communicatie, Betaling, Wachtlijst","evCreated":"","evPrijsVerplicht":"1","evAangemaakt":"2018-03-16 14:10:38","evAangepast":"2018-03-16 14:16:33","evReadonlyDN":"0","evClusterKeuzeVraag":"0","evClusterAantalVraag":"0"}';
$data = json_decode($input);   

$sql = "UPDATE ".$tabelNaam." SET ";

foreach($data as $key => $value) {
  if($key == $idValue){
      $idValue = $value;
  } else {
      if ($value == '') {
          $sql= $sql.$key." = null,";    
      } else {      
          $sql= $sql."$key ='$value',";             
      }  
  }
}

$sql = substr($sql,0, strlen($sql) - 1 ).
        ' WHERE '.$idName." = ".$idValue;

echo $sql;

echo "<BR>";

if ($dbConnection->query($sql) === TRUE) {
    $feedback = '[{"update":"OK"}]';
} else {
    $feedback = '[{"update":"error"}]';
}

$dbConnection->close();

echo $feedback;

