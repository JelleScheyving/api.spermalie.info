<?php
//create evenement
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object file
include_once '../config/database.php';
include_once '../_objects/evenement.php';
include_once '../utilities/controlMessage.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate product object
$evenement = new Evenement($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

// set event property values
$evenement->evEventID = $data->evEventID;
$evenement->evNaam = $data->evNaam;
$evenement->evDatum = $data->evDatum;
$evenement->evAanvangsTijd = $data->evAanvangsTijd;
$evenement->evEindDatumInschrijving = $data->evEindDatumInschrijving;
$evenement->evOmschrijving = $data->evOmschrijving;
$evenement->evMaxDeelnemers = $data->evMaxDeelnemers;
$evenement->evMaxInschrijvingen = $data->evMaxInschrijvingen;
$evenement->evAfkortingNaam = $data->evWachtwoord;
$evenement->evWachtwoord = $data->evWachtwoord;
$evenement->evSlotformule = $data->evSlotformule;
$evenement->evActief = $data->evActief;
$evenement->evTitelPrijzen = $data->evTitelPrijzen;
$evenement->evHerinneringstermijn = $data->evHerinneringstermijn;
$evenement->evBetalingstermijn = $data->evBetalingstermijn;
$evenement->evAnnulatieTermijn = $data->evAnnulatieTermijn;
$evenement->evMailBody = $data->evMailBody;
$evenement->evMailFrom = $data->evMailFrom;
$evenement->evMailSubject = $data->evMailSubject;
$evenement->evMailAttachment = $data->evMailAttachment;
$evenement->evHerinneringBody1 = $data->evHerinneringBody1;
$evenement->evHerinneringSubject = $data->evHerinneringSubject;
$evenement->evAnnuleringBody1 = $data->evAnnuleringBody1;
$evenement->evAnnuleringSubject = $data->evAnnuleringSubject;
$evenement->evRptBevestigingID = $data->evRptBevestigingID;
$evenement->evWachtlijst = $data->evWachtlijst;
$evenement->evAanhefRapport = $data->evAanhefRapport;
$evenement->evTarieven = $data->evTarieven;
$evenement->evActiefVanaf = $data->evActiefVanaf;
$evenement->evRekeningNummer = $data->evRekeningNummer;
$evenement->evMailBcc = $data->evMailBcc;
$evenement->evLogoID = $data->evLogoID;
$evenement->evMailVerantwoordelijke = $data->evMailVerantwoordelijke;
$evenement->evCheckMaximum = $data->evCheckMaximum;
$evenement->evCheckNoMaxima = $data->evCheckNoMaxima;
$evenement->evSuccesBoodschap = $data->evSuccesBoodschap;
$evenement->evLogin = $data->evLogin;
$evenement->evBetalend = $data->evBetalend;
$evenement->evBevestiging = $data->evBevestiging;
$evenement->evTabs = $data->evTabs;
$evenement->evCreated = $data->evCreated;
$evenement->evPrijsVerplicht = $data->evPrijsVerplicht;
$evenement->evAangemaakt = $data->evAangemaakt;
$evenement->evAangepast = $data->evAangepast;
$evenement->evReadonlyDN = $data->evReadonlyDN;
$evenement->evClusterKeuzeVraag = $data->evClusterKeuzeVraag;
$evenement->evClusterAantalVraag = $data->evClusterAantalVraag;



// create the product
$newid = $evenement->create();

if(!$newid){
    // json format output
    echo '[{"newid":"error"}]';
}
// if able to create the product, tell the user
else{
    
    // create array
    $instances_arr = array(
        "evEventID" => $newid,
        "evNaam "=> $evNaam,
        "evDatum "=> $evDatum,
        "evAanvangsTijd "=> $evAanvangsTijd,
        "evEindDatumInschrijving "=> $evEindDatumInschrijving,
        "evOmschrijving "=> $evOmschrijving,
        "evMaxDeelnemers "=> $evMaxDeelnemers,
        "evMaxInschrijvingen "=> $evMaxInschrijvingen,
        "evAfkortingNaam "=> $evAfkortingNaam,
        "evWachtwoord "=> $evWachtwoord,
        "evSlotformule "=> $evSlotformule,
        "evActief "=> $evActief,
        "evTitelPrijzen "=> $evTitelPrijzen,
        "evHerinneringstermijn "=> $evHerinneringstermijn,
        "evBetalingstermijn "=> $evBetalingstermijn,
        "evAnnulatieTermijn "=> $evAnnulatieTermijn,
        "evMailBody "=> $evMailBody,
        "evMailFrom "=> $evMailFrom,
        "evMailSubject "=> $evMailSubject,
        "evMailAttachment "=> $evMailAttachment,
        "evHerinneringBody1 "=> $evHerinneringBody1,
        "evHerinneringSubject "=> $evHerinneringSubject,
        "evAnnuleringBody1 "=> $evAnnuleringBody1,
        "evAnnuleringSubject "=> $evAnnuleringSubject,
        "evRptBevestigingID "=> $evRptBevestigingID,
        "evWachtlijst "=> $evWachtlijst,
        "evAanhefRapport "=> $evAanhefRapport,
        "evTarieven "=> $evTarieven,
        "evActiefVanaf "=> $evActiefVanaf,
        "evRekeningNummer "=> $evRekeningNummer,
        "evMailBcc "=> $evMailBcc,
        "evLogoID "=> $evLogoID,
        "evMailVerantwoordelijke "=> $evMailVerantwoordelijke,
        "evCheckMaximum "=> $evCheckMaximum,
        "evCheckNoMaxima "=> $evCheckNoMaxima,
        "evSuccesBoodschap "=> $evSuccesBoodschap,
        "evLogin "=> $evLogin,
        "evBetalend "=> $evBetalend,
        "evBevestiging "=> $evBevestiging,
        "evTabs "=> $evTabs,
        "evCreated "=> $evCreated,
        "evPrijsVerplicht "=> $evPrijsVerplicht,
        "evAangemaakt "=> $evAangemaakt,
        "evAangepast "=> $evAangepast,
        "evReadonlyDN "=> $evReadonlyDN,
        "evClusterKeuzeVraag "=> $evClusterKeuzeVraag,
        "evClusterAantalVraag "=> $evClusterAantalVraag
    );
    sleep(1);
    // make it json format
    print_r(json_encode($instances_arr));
}