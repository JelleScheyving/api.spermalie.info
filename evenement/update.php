<?php
//evenement
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$objectNaam = 'evenement';
$tabelNaam = 'tbl_evenement';
$idName = "evEventID";

// include database and object file 
include_once '../config/mysqli.php'; 
include_once '../_objects/'.$objectNaam.'.php'; 

// get database connection 
$mySqlIConnection = new MySqlIConnection(); 
$dbConnection = $mySqlIConnection->getConnection();
 
// get database connection 

$idValue = 0;
$feedback = "OK";
 
$instance = new Evenement($dbConnection);

$input = file_get_contents("php://input");
$data = json_decode($input);   

$sql = "UPDATE ".$tabelNaam." SET ";

foreach($data as $key => $value) {
  if($key == $idValue){
      $idValue = $value;
  } else {
      if ($value == '') {
          $sql= $sql.$key." = null,";    
      } else {      
          $sql= $sql."$key ='$value',";             
      }  
  }
}

$sql = substr($sql,0, strlen($sql) - 1 ).
        ' WHERE '.$idName." = ".$idValue;

if ($dbConnection->query($sql) === TRUE) {
    $feedback = '[{"update":"OK"}]';
    //$feedback = '{"update":"OK"}';
    //$feedback = 'OK';
} else {
    $feedback = '{"update":"error"}';
    //$feedback = '[{"update":"error"}]';
    //$feedback = 'Error';
}

$dbConnection->close();

echo $feedback;

