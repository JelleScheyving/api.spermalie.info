<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$className = 'evenement';

// include database and object files
include_once '../utilities/carriageReturn.php';
include_once '../config/database.php';
include_once '../_objects/'.$className.'.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
// #### pas naam object aan
$evenement = new Evenement($db);

// query products
$arrInstances = $evenement->readAll();
$num = $arrInstances->rowCount();
 
$data="";
 
// check if more than 0 record found
if($num>0){
 
    $x=1;
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $arrInstances->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $data .= '{';
            $data .= '"evEventID":"' . parse($evEventID) . '",';
            $data .= '"evNaam":"' . parse($evNaam) . '",';
            $data .= '"evDatum":"' . parse($evDatum) . '",';
            $data .= '"evAanvangsTijd":"' . parse($evAanvangsTijd) . '",';
            $data .= '"evEindDatumInschrijving":"' . parse($evEindDatumInschrijving) . '",';
            $data .= '"evOmschrijving":"' . parse($evOmschrijving) . '",';
            $data .= '"evMaxDeelnemers":"' . parse($evMaxDeelnemers) . '",';
            $data .= '"evMaxInschrijvingen":"' . parse($evMaxInschrijvingen) . '",';
            $data .= '"evAfkortingNaam":"' . parse($evAfkortingNaam) . '",';
            $data .= '"evWachtwoord":"' . parse($evWachtwoord) . '",';
            $data .= '"evSlotformule":"' . parse($evSlotformule) . '",';
            $data .= '"evActief":"' . parse($evActief) . '",';
            $data .= '"evTitelPrijzen":"' . parse($evTitelPrijzen) . '",';
            $data .= '"evHerinneringstermijn":"' . parse($evHerinneringstermijn) . '",';
            $data .= '"evBetalingstermijn":"' . parse($evBetalingstermijn) . '",';
            $data .= '"evAnnulatieTermijn":"' . parse($evAnnulatieTermijn) . '",';
            $data .= '"evMailBody":"' . parse($evMailBody) . '",';
            $data .= '"evMailFrom":"' . parse($evMailFrom) . '",';
            $data .= '"evMailSubject":"' . parse($evMailSubject) . '",';
            $data .= '"evMailAttachment":"' . parse($evMailAttachment) . '",';
            $data .= '"evHerinneringBody1":"' . parse($evHerinneringBody1) . '",';
            $data .= '"evHerinneringSubject":"' . parse($evHerinneringSubject) . '",';
            $data .= '"evAnnuleringBody1":"' . parse($evAnnuleringBody1) . '",';
            $data .= '"evAnnuleringSubject":"' . parse($evAnnuleringSubject) . '",';
            $data .= '"evRptBevestigingID":"' . parse($evRptBevestigingID) . '",';
            $data .= '"evWachtlijst":"' . parse($evWachtlijst) . '",';
            $data .= '"evAanhefRapport":"' . parse($evAanhefRapport) . '",';
            $data .= '"evTarieven":"' . parse($evTarieven) . '",';
            $data .= '"evActiefVanaf":"' . parse($evActiefVanaf) . '",';
            $data .= '"evRekeningNummer":"' . parse($evRekeningNummer) . '",';
            $data .= '"evMailBcc":"' . parse($evMailBcc) . '",';
            $data .= '"evLogoID":"' . parse($evLogoID) . '",';
            $data .= '"evMailVerantwoordelijke":"' . parse($evMailVerantwoordelijke) . '",';
            $data .= '"evCheckMaximum":"' . parse($evCheckMaximum) . '",';
            $data .= '"evCheckNoMaxima":"' . parse($evCheckNoMaxima) . '",';
            $data .= '"evSuccesBoodschap":"' . parse($evSuccesBoodschap) . '",';
            $data .= '"evLogin":"' . parse($evLogin) . '",';
            $data .= '"evBetalend":"' . parse($evBetalend) . '",';
            $data .= '"evBevestiging":"' . parse($evBevestiging) . '",';
            $data .= '"evTabs":"' . parse($evTabs) . '",';
            $data .= '"evCreated":"' . parse($evCreated) . '",';
            $data .= '"evPrijsVerplicht":"' . parse($evPrijsVerplicht) . '",';
            $data .= '"evAangemaakt":"' . parse($evAangemaakt) . '",';
            $data .= '"evAangepast":"' . parse($evAangepast) . '",';
            $data .= '"evReadonlyDN":"' . parse($evReadonlyDN) . '",';
            $data .= '"evClusterKeuzeVraag":"' . parse($evClusterKeuzeVraag) . '",';
            $data .= '"evClusterAantalVraag":"' . parse($evClusterAantalVraag) . '"';
        $data .= '}';
 
        $data .= $x<$num ? ',' : '';
 
        $x++;
    }
}
 
// json format output
echo '[' . parse($data) . ']';